apt-get -y update
apt-get -y upgrade

echo "-->Installing sudo..."
apt-get install -y sudo
echo "-->sudo installed"

echo "-->Installing gcc..." 
sudo apt-get -y install gcc
echo "-->gcc installed"

echo "-->Installing g++ ..."
sudo apt-get -y install g++ 
echo "-->g++ installed"

echo "-->Installing git..."
sudo apt-get -y install git
echo "-->git installed"

echo "-->Installing wget ..."
sudo apt-get -y install wget
echo "-->wget installed"

echo "-->Installing curl..."
sudo apt-get -y install curl
echo "-->wget installed"

echo "-->Installing libssl-dev library..."
sudo apt-get -y install libssl-dev
echo "-->Library libssl-dev installed"

apt-get -y update

echo "-->Installing build-essentials ..."
sudo apt-get -y install build-essential
echo "-->build-essential installed"

echo "-->Setting timezone to Athens Greece UTC+3"
ln -fs /usr/share/zoneinfo/Europe/Athens /etc/localtime

echo "-->Installing cmake..."
sudo apt-get -y install cmake
echo "-->cmake installed"

echo "-->Installing python 2...."
sudo apt-get -y install python
echo "-->Python 2 installed"

echo "-->Installing pip..."
sudo apt update
curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py
sudo python2 get-pip.py
echo "-->pip installed"

echo "-->Installing python3..."
sudo apt-get -y install python3
echo "-->python3 installed"

echo "-->Installing pyside2..."
pip install pyside2
echo "-->pyside2 installed"

echo "-->Installing PyOpenGL..."
pip install PyOpenGL
echo "-->PyOpenGL installed"

echo "-->Clone USD repository..."
cd /home
git clone https://github.com/PixarAnimationStudios/USD

echo "-->Installing some libraries..."
sudo apt-get -y install libtbb-dev
sudo apt-get -y install python-dev
sudo apt-get -y install libx11-dev
sudo apt-get update
sudo apt-get -y install mesa-common-dev
sudo apt-get update
sudo apt-get -y install libgl1-mesa-dev
sudo apt-get update
sudo apt-get -y install libglu1-mesa-dev
sudo apt-get install -y libglew-dev
sudo apt-get update
echo "-->libraries installed"

echo "-->Installing python3-opencv..."
sudo apt install -y python3-opencv
echo "-->python3-opencv installed"

echo "-->Installing PyOpenGl_accelerate..."
pip install PyOpenGL PyOpenGl_accelerate
echo "-->PyOpenGl_accelerate installed"

echo "-->Installing USD and dependencies..."
echo "-->May take half an hour or more depends on the system!"
python USD/build_scripts/build_usd.py /usr/local/USD
echo "-->USD installed"

echo "-->Installing nasm..."
sudo apt-get install -y nasm
echo "-->nasm installed"

echo "-->Installing Pillow..."
pip install Pillow
echo "-->Pillow installed"

echo "-->Clone usd_from_gltf repository"
git clone https://github.com/google/usd_from_gltf.git

echo "-->Installing usd_from_gltf"
python usd_from_gltf/tools/ufginstall/ufginstall.py /usr/local/UFG /usr/local/USD --testdata

echo "-->Installation finished!"
echo "-->The Executable is placed at /usr/local/UFG/bin/. (Executable: usd_from_gltf)"
echo "-->After you move there >./usd_from_gltf source_file destination_file"